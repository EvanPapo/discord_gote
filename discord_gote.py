import random
import discord
from discord.ext.commands import Bot
from gote_auth import TOKEN
import cleverbotfree.cbfree



client = discord.Client()
BOT_PREFIX = ('!')
client = Bot(command_prefix=BOT_PREFIX, case_insensitive=True)
replacementWords = ['butt', 'butts', 'my butt']
cb = cleverbotfree.cbfree.Cleverbot()


@client.event
async def on_ready():
    await client.change_presence(activity=discord.Game('with kids'))
    client.isMuted = False
    client.chanceOfButt = 0.03
    print('I am ' + str(client.user))
    print('Logged in as ' + client.user.name)
    
    
@client.event
async def on_message(message):
    if not client.isMuted:
        if message.author == client.user:
            return

        #if message.content.startswith('$hello'):
            # message.author.name: server-specific nickname
            # str(message.author): name#number
            # str(message.author.id): integer (not the same as the #number)
        #    await message.channel.send('Hello! ' + str(message.author.display_name))  # nickname
        
        if '<3' in message.content or '</3' in message.content:
            await message.channel.send(random.choice(['<3', '</3']))
        
        if random.random() < client.chanceOfButt and not message.content.startswith(BOT_PREFIX):
            # replacement will be made
            words = message.content.split(' ')
            word_index = random.randint(0, len(words))
            msg = ' '.join(words[:word_index] + [random.choice(replacementWords)] + words[word_index+1:])
            await message.channel.send(msg)
        
        if "gote" in message.content.lower() or random.random() < 0.05:
            query = message.content if message.content else ' '
            print("Contacting Cleverbot for: " + message.content)
            #cb = cleverbotfree.cbfree.Cleverbot()
            response = cb.single_exchange(message.content)
            #cb.browser.close()
            await message.channel.send(response)
        
        
    await client.process_commands(message)
    
    
@client.command(name='8ball',
                description='Answers a yes/no question.',
                brief='Answers from the beyond.',
                aliases=['eight_ball', 'eightball', '8-ball'],
                pass_context=True)
async def eight_ball(ctx):
    possible_responses = [
        'That is a resounding no',
        'It is not looking likely',
        'Too hard to tell',
        'It is quite possible',
        'Definitely',
    ]
    if not client.isMuted:
        await ctx.send(random.choice(possible_responses) + ', ' + ctx.message.author.mention)
    
    
@client.command(name='butt',
                description='Sets current chance of butt',
                brief='Sets current chance of butt',
                aliases=[],
                pass_context=True)
async def butt(ctx, number):
    client.chanceOfButt = float(number) if (0 <= float(number) <= 1) else 0.1
    await ctx.send('Chance of butt has been set to ' + str(client.chanceOfButt))
    
    
@client.command(name='chanceOfButt',
                description='Shows current chance of butt',
                brief='Shows current chance of butt',
                aliases=['cbutt'],
                pass_context=True)
async def chanceOfButt(ctx):
    await ctx.send('Chance of butt is ' + str(client.chanceOfButt))
    
    
@client.command(name='hello',
                description='Says hello back.',
                brief='Says hello back.',
                aliases=['hi', 'yo'],
                pass_context=True)
async def hello(ctx):
    if not client.isMuted:
        await ctx.send('Hello, ' + ctx.message.author.mention)
    
    
@client.command(name='mute',
                description='Mutes bot.',
                brief='Mutes bot.',
                aliases=['shutup'],
                pass_context=True)
async def mute(ctx):
    client.isMuted = True
    
    
@client.command(name='mutestatus',
                description='Shows mute status.',
                brief='Shows mute status.',
                aliases=['status'],
                pass_context=True)
async def mutestatus(ctx):
    await ctx.send('I am ' + ('muted.' if client.isMuted else 'unmuted.'))
    
    
@client.command(name='unmute',
                description='Unmutes bot.',
                brief='Unmutes bot.',
                aliases=['comeback'],
                pass_context=True)
async def unmute(ctx):
    client.isMuted = False
    
    
@client.command(name='random',
                description='Generates random number between set limits. E.g. !random 10, !random 5 20',
                brief='Generates random number',
                pass_context=True)
async def ranNumber(ctx):
    argStr = ctx.message.content.split(' ', 1)[1].strip()
    nums = argStr.split(' ')
    failMessage = False
    lowerLimit = 1
    upperLimit = 1
    
    if len(nums) == 1:
        try:
            upperLimit = int(nums[0])
        except:
            failMessage = 'Not an integer.'
    elif len(nums) == 2:
        try:
            lowerLimit = int(nums[0])
            upperLimit = int(nums[1])
        except:
            failMessage = 'Not all arguments are integers.'
    else:
        failMessage = 'Unexpected number of arguments, give 1 or 2.'
    
    if not failMessage:
        await ctx.send(random.randint(lowerLimit, upperLimit))
    else:
        await ctx.send(failMessage)
    
    
client.run(TOKEN)
